# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Set path for ~/.scriptis folder
export PATH="$PATH:$(du "$HOME/.scripts/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"

# Path to your oh-my-zsh installation.
export ZSH="/home/samuel/.oh-my-zsh"

export _JAVA_AWT_WM_NONREPARENTING=1

if [ "$(tty)" = "/dev/tty1" ]; then
    exec startx
fi


# Alias

alias mplayerfb='mplayer -vo fbdev2 -vf scale=1366:768'
alias java8='/usr/lib/jvm/java-8-openjdk/jre/bin/java'
# alias tic80='cd ~/TIC-80 && ./tic80 .'
alias vim='nvim'
# alias ls='lsd'
alias neolol='neofetch | lolcat'
alias pac='sudo pacman'
alias paclear='sudo pacman -Rcs $(pacman -Qdtq)'
# alias cow='fortune | cowsay | lolcat'
alias rickg='youtube-dl -o- "https://www.youtube.com/watch?v=dQw4w9WgXcQ" | mpv -'
alias rickc='curl -s -L http://bit.ly/10hA8iC | bash'
alias darudeg='youtube-dl -o- "https://www.youtube.com/watch?v=y6120QOlsfU" | mpv -'
alias darudec='youtube-dl -o- "https://www.youtube.com/watch?v=y6120QOlsfU" | mplayer -vo caca -'
alias crabrc='youtube-dl -o- "https://www.youtube.com/watch?v=LDU_Txk06tM" | mplayer -vo caca -'

alias cpuperf='sudo cpupower frequency-set -g performance'

alias config='/usr/bin/git --git-dir=$HOME/DeboniDots/ --work-tree=$HOME'

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

# ZSH_THEME="bira"
# ZSH_THEME="duellj"
ZSH_THEME="fino"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='vim'
 else
   export EDITOR='nvim'
 fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

source /home/samuel/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
